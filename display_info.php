<?php 
session_start();
// print_r($_SESSION);
$sex = array(
    "Nam",
    "Nữ"
);

$department = array(
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu"
);

$sexName = $sex[$_SESSION["sex"]];
$departmentName = $department[$_SESSION["department"]];
$birthday = $_SESSION["birthday"];
$address = $_SESSION["address"];
// $img = $_SESSION["img_encode"];
$img = $_SESSION['img_dir'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>

    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Day 05</title>
</head>

<body>
    <div class="wrapper">
        <form  enctype="multipart/form-data">
            <div class="input_display">
                <label >Họ và tên</label>
                <?php 
                    echo "<p>$_SESSION[name]</p>" 
                ?>
            </div>

            <div class="input_display">
                <label >Giới tính</label>
                <?php
                    echo "
                    <p>$sexName</p>
                    ";
                ?>
            </div>

            <div class="input_display">
                <label>Phân khoa</label>
                <?php
                    echo "
                    <p style=\"padding-right:100px\">$departmentName</p>
                    ";
                ?>
            </div>

            <div class="input_display">
                <label >Ngày sinh</label>
                                
                <?php
                    echo "
                    <p>$birthday</p>
                    ";
                ?>
            </div>

            <div class="input_display">
                <label>Địa chỉ</label>
                <?php
                    echo "
                    <p>$address</p>
                    ";
                ?>
            </div>

            <div class="img__input_display">
                <label for="">Hình ảnh</label>
                <?php
                echo "
                <img src='$img' alt='' class=\"img_display\">";
                ?>
            </div>
            <button type="submit">Xác nhận</button>
        </form>
    </div>
</body>

</html>
